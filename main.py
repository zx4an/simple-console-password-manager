import sqlite3

conn = sqlite3.connect('passwords.db')
cur = conn.cursor()

while True:
    print('Select an option')
    print('1. Add a password')
    print('2. Choose password')
    print('3. Remove password')

    choice = int(input())

    if choice == 1:
        login = input('Enter your email/login: ')
        password = input('Enter your password: ')
        name = input('Enter the name: ')

        cur.execute("SELECT DISTINCT tag FROM passwords")

        tags = cur.fetchall()

        print('choose a platform:')
        for idx, tag in enumerate(tags):
            print(f'{idx + 1}. {tag[0]}')
        print(f'{len(tags) + 1}. Add new')
        platform_choice = int(input())

        if platform_choice == len(tags) + 1:
            platform = input('Enter the new platform name: ')
        else:
            platform = tags[platform_choice - 1][0]

        cur.execute("INSERT INTO passwords(password, login, tag, name) VALUES (?, ?, ?, ?)", (password, login, platform, name,))

        conn.commit()


    elif choice == 2:

        cur.execute("SELECT DISTINCT tag FROM passwords")
        tags = cur.fetchall()

        print('Choose the platform:')
        for idx, tag in enumerate(tags):
            print(f'{idx + 1}. {tag[0]}')

        platform_choice = tags[int(input()) - 1][0]

        cur.execute("SELECT name FROM passwords WHERE tag = ?", (platform_choice,))

        names = cur.fetchall()

        print('Choose the platform:')
        for idx, name in enumerate(names):
            print(f'{idx + 1}. {name[0]}')

        name_choice = names[int(input()) - 1][0]

        cur.execute("SELECT password, login FROM passwords WHERE tag = ? AND name = ?", (platform_choice, name_choice,))

        passwords = cur.fetchall()

        for password in passwords:
            print(f'Login: {password[1]} || Password: {password[0]}')


    elif choice == 3:

        cur.execute("SELECT DISTINCT tag FROM passwords")
        tags = cur.fetchall()

        print('Choose the platform:')
        for idx, tag in enumerate(tags):
            print(f'{idx + 1}. {tag[0]}')

        platform_choice = tags[int(input()) - 1][0]

        cur.execute("SELECT name FROM passwords WHERE tag = ?", (platform_choice,))

        names = cur.fetchall()

        print('Choose the platform:')
        for idx, name in enumerate(names):
            print(f'{idx + 1}. {name[0]}')

        name_choice = names[int(input()) - 1][0]

        cur.execute("DELETE FROM passwords WHERE tag = ? AND name = ?", (platform_choice, name_choice,))

        conn.commit()

    else:
        exit()

    repeat = input('Would you like to do something else (y/n): ')

    if repeat.lower() != 'y':
        break

conn.close()